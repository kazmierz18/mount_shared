#!/bin/bash
#-----------------------------------------------------------------------------
# Discover VirtualBox shared folders and mount them if it makes sense
#-----------------------------------------------------------------------------

if  !  type  VBoxControl  > /dev/null;  then
  echo  'VirtualBox Guest Additions NOT found'  > /dev/stderr
  exit 1
fi

MY_UID="$(id -u)"
MY_GID="$(id -g)"
FOLDER_LIST="$(sudo  VBoxControl  sharedfolder  list| grep '^ *[0-9][0-9]* *- *' | sed  -e 's/^ *[0-9][0-9]* *- *//')"
for SHARED_FOLDER in $FOLDER_LIST
do
  echo "Found: $SHARED_FOLDER"
  MOUNT_POINT="$HOME/$SHARED_FOLDER"
  if  [ -d "$MOUNT_POINT" ];  then
	if mountpoint -q "$MOUNT_POINT" ; then
		echo  "Already mounted:  $MOUNTED"	
	else
		if [ "$(ls -A "$MOUNT_POINT")" ]; then
			echo "Cant mount $SHARED_FOLDER. Folder $MOUNT_POINT is not empty"
		else
			echo "Mounting at $MOUNT_POINT"
			sudo  mount  -t vboxsf  -o nosuid,uid="$MY_UID",gid="$MY_GID" "$SHARED_FOLDER" "$MOUNT_POINT"
		fi
	fi
  else
    echo "Mounting at $MOUNT_POINT"
    mkdir "$MOUNT_POINT"
    sudo  mount  -t vboxsf  -o nosuid,uid="$MY_UID",gid="$MY_GID" "$SHARED_FOLDER" "$MOUNT_POINT"
  fi
done
